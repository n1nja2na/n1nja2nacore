--GLOBALS IS USED FOR ANYTHING THAT USES N1NJA2NACORE

--Basic Structure
n1nja2naCore = {}
n1nja2naCore.Name = "n1nja2naCore"
n1nja2naCore.Author = "n1nja2na"

--The text sizes we can use
n1nja2naCore.TextSizes = {}
n1nja2naCore.TextSizes.Small = CSA_EVENT_SMALL_TEXT
n1nja2naCore.TextSizes.Large = CSA_EVENT_LARGE_TEXT
n1nja2naCore.TextSizes.Other = CSA_EVENT_COMBINED_TEXT

--The sounds we can use identified by sound 
n1nja2naCore.Sounds = {}
n1nja2naCore.Sounds.Drum = "AvA_Gate_Closed"
n1nja2naCore.Sounds.LFG = "LFG_Search_Started"



