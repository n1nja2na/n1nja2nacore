--METHODS FOR N1NJA2NACORE


--SEND A NOTIFICATION TO THE SCREEN
function n1nja2naCore.SendNotification(message, textSize, sound)
	CENTER_SCREEN_ANNOUNCE:AddMessage(0, textSize, sound, message, nil, nil, nil, nil, nil, 2*1000, nil)
end

--SPLIT A STRING BY " "
function n1nja2naCore.Split(text)
	arr = {}
	index = 1
	for i in string.gmatch(text, "%S+") do
		arr[index] = i
		index = index + 1	
	end
	return arr
end

--CHANGE A NUMBER LIKE 100000000 to 100,000,000
function n1nja2naCore.comma_value(amount) --found at http://lua-users.org/wiki/FormattingNumbers
	  local formatted = amount
	  while true do
	    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
	    if (k==0) then
	      break
	    end
	  end
	  return formatted
end

--IM LAZY SO I MADE AN EASY WAY TO RELOAD THE UI
function n1nja2naCore.ReloadUIShortHand()
	StartChatInput("/reloadui")
	CHAT_SYSTEM:SubmitTextEntry()
end SLASH_COMMANDS["/rr"] = n1nja2naCore.ReloadUIShortHand


--ADD TO SLASH COMMANDS
function n1nja2naCore.addCommands(dict)
	for k,v in pairs(dict) do
		SLASH_COMMANDS[k] = v
	end

end
